# Formation Liesse : Intelligence artificielle en Science Industrielles de l’Ingénieur. Les bases autour d'applications concrètes

L’intelligence artificielle est devenue très populaire ces dernières années dans de nombreux champs d’applications : l’étude statistique de données, le traitement d’images ou encore la commande de systèmes.
L'objectif de cette formation est de maitriser les algorithmes d'apprentissage machine présents dans les programmes de CPGE publié au BO du 7-2-2021.

La formation du 22 et 23 juin 2022 à été enregistré.
Les 4 demies journées sont disponibles sur mon drive : [lien](https://drive.google.com/file/d/1wsKFL1xpbHh9VjkpGDa5LBYM2xJz5jLZ/view?usp=sharing)

# Sur ce git
- Les supports utilisés lors de la formation
- Les codes des travaux pratiques