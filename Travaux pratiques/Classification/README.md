# Classification
La classification automatique est la catégorisation algorithmique d'observation. Elle consiste à attribuer une classe ou catégorie à chaque observation à classer (c'est à dire un point de la base donnée), en se fondant sur des données statistiques.


## Liste de TP proposés
Nous verrons deux types de classification :
1. Machine Learning : K plus proche voisin <img src="../../../img/python.png" height="18"/>
2. Réseau de neurones :
    1. Détection de défaut <img src="../../../img/python.png" height="18"/>
    2. Classification de chiffres en écriture manuscrite <img src="../../../img/python.png" height="18"/> <img src="../../../img/colab.png" height="18"/>
    3. Segmentation d'image routière pour la conduite autonome <img src="../../../img/python.png" height="18"/> <img src="../../../img/colab.png" height="18"/>