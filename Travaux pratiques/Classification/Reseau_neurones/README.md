# Classification avec r�seau de neurones
Nous proposons deux activit�es d'une difficult� in�gale.

## Classification de chiffres <img src="../../../img/python.png" width="25"/> <img src="../../../img/colab.png" width="25"/>
Dans les ann�es 1990, le cr�dit mutuel de Bretagne, aid� de Yann Le Cun son �quipe, a mis en place une solution de lecture automatique des �critures manuscrites sur les ch�ques.
Ils ont utilis� un algorithme de r�seau de neurones.
�tudions comment identifier des chiffres manuscrits.

## Segmentation d'images <img src="../../../img/python.png" width="25"/> <img src="../../../img/colab.png" width="25"/>
Ce TP propose de visualiser le fonctionnement d'un r�seau convolutif r�alisant de la segmentation d'image.
Le r�seau est entrain� sur des images de circulation et doit reconnaitre les parties de l'image correspondant � une route, un trottoire, un pi�ton, un cycliste ...

## D�tection de d�faut <img src="../../../img/python.png" width="25"/>
D�tecter des d�fauts � partir de s�ries temporelles synth�tiques.
Ces s�ries repr�sentent les donn�es acquisent par un capteur.

*TP non termin�* : Il faudrait ajouter des donn�es issues d'enregistrements sur un syst�me r�el.