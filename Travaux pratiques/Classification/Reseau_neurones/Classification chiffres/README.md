# Classification de chiffres <img src="../../../../img/python.png" height="25"/> <img src="../../../../img/colab.png" height="25"/>

## Plateforme utilis�e
Vous pouvez utiliser :
- Google Colab <img src="../../../../img/colab.png" height="15"/> et ses serveurs (Aucune installation n�cessaire) : [lien](https://colab.research.google.com/drive/1PfCocp0cuWmh1oiAiDSjj4csyEINDu6Y?usp=sharing).
- Votre propore installation python <img src="../../../../img/python.png" height="15"/> et les fichiers de ce r�pertoire.
Voir page d'acceuil pour les conseils d'installation: [ici](https://gitlab.com/Guenole.cherot/liesse/-/tree/master/Travaux%20pratiques).

## But du TP
Dans les ann�es 1990, le cr�dit mutuel de Bretagne, aid� de Yann Le Cun son �quipe, a mis en place une solution de lecture automatique des �critures manuscrites sur les ch�ques.
Ils ont utilis� un algorithme de r�seau de neurones.
�tudions comment identifier des chiffres manuscrits.

## Points du programme abord�s
- [x] Classification
- [x] R�seau de neurones
- [x] Notion de sur-apprentissage