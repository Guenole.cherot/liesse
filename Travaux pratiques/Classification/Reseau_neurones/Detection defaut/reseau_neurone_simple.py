# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 17:02:32 2022

@author: Guenole
"""
import numpy as np
from tensorflow import keras
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv1D, MaxPooling1D, LSTM, Normalization, Flatten
from tensorflow.keras.utils import to_categorical
from sklearn.model_selection import train_test_split
from os.path import join, dirname

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# Reproducibility
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
rs = RandomState(MT19937(SeedSequence(2))) # Fixe l'aléatoire

##############################################################################
################################ COEUR DU CODE ###############################
##############################################################################

def generate_data(n_obs, n_step, ratio):
    """Création des séries temporelles issue d'un capteur.
    Chacune des séries provient soit d'un système normal soit un système défectueux."""
    # Créer la série temporelle
    X = rs.normal(size=(n_obs, n_step)) # Vecteur qui représente l'échantillonnage du capteur
    tri = np.ones((n_step, n_step)) # Matrice pour créer la série temporelle
    tri = np.triu(tri) # Matrice trianguaire supérieure
    X = np.matmul(X, tri) # Création de la série temporelle "normale"
    
    # Créer les étiquettes
    y = np.zeros(n_obs) # Création des étiquettes
    rand = rs.choice(np.arange(0,n_obs, 1), int(n_obs*ratio), replace=False) # Identifiant des séries défecteuses
    y[rand] = 1 # Série temporelles défectueuses
    freq = 0.5 # Fréquance des oscillations à àjouter aux séries temporelles défecteuses
    default = np.sin(np.arange(0, n_step*freq, freq)) # Création du défaut (toujours le même ici : oscillation sinus de freq fixe)
    X[rand] += 0.5*default # Ajout du défaut aux séries temporelles défectueuses
    return X, y

def train(X, y, validation_data):
    """Entraine le réseau de neurone à classifier les séries temporelles défectueuses."""
    # Normalisation des données
    norm = Normalization(axis=None) # Instance de l'outil de normalisation de TensorFlow
    norm.adapt(X) # Extrait les statistiques de X (moyenne, écart type, ...)
    X = norm(X) # Normalise les données (elles peuvent être "dé-normalisé" ensuite si on sauvegarde norm)

    # Création du modèle
    model = create_model_FC(X.shape[1], simple=False) # Créer le réseau de neurones totalement connecté
    model.summary() # Affiche un résumé du réseau de neurones
    
    # Fonction de rappel (ou Callback). Se rappelle des poids du réseau qui avait la meilleur performance sur val_loss. Evite l'overfitting.
    callback = tf.keras.callbacks.EarlyStopping(monitor="val_loss", patience=10, restore_best_weights=True)

    model.compile(optimizer=keras.optimizers.Adam(learning_rate=8e-6), # Algorithme utilisé pour la back-propagation
                  loss=keras.losses.CategoricalCrossentropy(),
                  metrics=['accuracy', tf.keras.metrics.Precision(), tf.keras.metrics.Recall()]) # Fonction coût
    history = model.fit(X, y, validation_data=validation_data, batch_size=128, epochs=30, callbacks=[callback])
    
    return model, norm, history

def create_model_FC(input_size, simple=False):
    """Créer un modèle avec un réseau totalement connecté.
    Si simple == True : Créer un réseau de neuronnes avec 8 fois moins de neurones dans chaque couche."""
    # Si on veut un réseau simple on divise par 8 le nb de neuronnes
    if simple :
        norm = 8
    else :
        norm = 1
    # Créer le modèle
    model = Sequential() # Modèle séquentielle (ou feed-forward)
    model.add(Dense(int(256/norm), input_shape=(input_size,), activation="relu")) # Première couche (d'entrée)
    model.add(Dense(int(128/norm), activation="relu")) # Deuxième couche (cachée)
    model.add(Dense(int(64/norm), activation="relu")) # Troisième couche (cachée)
    model.add(Dense(2, activation="softmax")) # Quatrième couche (sortie)
    return model

def test(model, norm, X):
    """Teste le modèle en comparant les résultats théoriques et ceux prédits par le modèle."""
    X = norm(X) # Normalise les observation issue de la base de donneés
    y_pred = model.predict(X) # Utilise le réseau de neurone pour dire si la série temporelle présente un défaut ou non
    y_pred = y_pred.argmax(axis=1) # Remet en forme la sortie du réseau de neurone (Pas important)
    return y_pred

##############################################################################
################################## FIGURES ###################################
##############################################################################

def plot_observation(ax, X, y, k_max=4):
    """Affiche une observation de la base de donnée."""
    k_max = min(k_max, X.shape[0]) # Nombre de courbe à afficher
    for k in range(k_max): # Pour toute les courbes à afficher
        if y[k] == 1 : # Si la série temporelle est défectueuse
            color = "C3" # Donner la couleur "C3"
        else :
            color = "C2" # Sinon donner la couleur "C0"
        ax.plot(X[k], color=color) # Ajouter la courbe à la figure
    ax.set_ylabel("Position (cm)")
    ax.set_xlabel("Temps (ms)")
    ax.set_title("Séries temporelles issues du capteur de position")
    ax.set_xlim(xmin=0, xmax=len(X[0]))
    ax.grid(True)

def plot_fft_labeling(ax, X, y, k_max=10):
    """Est-ce qu'on peut faire mieux que l'IA avec une fft ?"""
    # Légende
    from matplotlib.lines import Line2D
    legend_elements = [Line2D([0], [0], color='C2', lw=4, label='Normal'),
                       Line2D([0], [0], color='C3', lw=4, label='Défectueux')]
    # Affichage des courbes
    X_fft = np.fft.fft(X, axis=1) # Transformée de Fourier discrète
    color = ["C2", "C3"] # Couleur des deux classes
    for k in range(k_max):
        ax.semilogy(np.abs(X_fft[k,0:30]), color=color[int(y[k])]) # Affiche la transformé de fourier de chaque observation
    ax.set_ylabel("Amplitude des harmoniques")
    ax.set_xlabel("Fréquence (Hz)")
    ax.set_title("Transformée de Fourier des séries temporelles")
    ax.legend(handles=legend_elements, loc='upper right')
    ax.set_xlim(xmin=0, xmax=30)
    ax.grid(True)

def plot_confusion_matrix(y_test, y_pred):
    """Affiche la matrice de confusion."""
    from sklearn.metrics import ConfusionMatrixDisplay
    from sklearn.metrics import confusion_matrix
    y_test = y_test.argmax(axis=1)
    labels = ["Normal", "Défaillant"]
    cm = confusion_matrix(y_test, y_pred, normalize='true')
    disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=labels)
    disp.plot(cmap=plt.cm.Blues)
    plt.show()
    
def plot_metric(ax, history, metric="accuracy", legend=False):
    ax.plot(history.history['{0}'.format(metric)])
    ax.plot(history.history['val_{0}'.format(metric)])
    ax.set_title('model {0}'.format(metric))
    ax.set_ylabel('{0}'.format(metric))
    ax.set_xlabel('epoch')
    if legend :
        ax.legend(['train', 'test'], loc='lower right')
    
def plot_loss(ax, history):
    ax.plot(history.history['loss'])
    ax.plot(history.history['val_loss'])
    ax.set_title('model loss')
    ax.set_ylabel('loss')
    ax.set_xlabel('epoch')


def plot(X, y, history):
    """Plot all figures."""
    gs = gridspec.GridSpec(4, 2)
    fig = plt.figure(figsize=(7,9), dpi=300)
    
    ax0 = fig.add_subplot(gs[0, :]) # ligne 0
    ax1 = fig.add_subplot(gs[1, :]) # ligne 1
    ax2 = fig.add_subplot(gs[2, 0])
    ax3 = fig.add_subplot(gs[2, 1])
    ax4 = fig.add_subplot(gs[3, 0])
    ax5 = fig.add_subplot(gs[3, 1])
    
    plot_observation(ax0, X, y) # Affiche une partie de la base de donnée
    plot_fft_labeling(ax1, X, y, k_max=10)
    plot_metric(ax2, history, metric="accuracy", legend=True)
    plot_loss(ax3, history)
    plot_metric(ax4, history, metric=list(history.history.keys())[2])
    plot_metric(ax5, history, metric=list(history.history.keys())[3])
    plt.tight_layout()
    path_folder = dirname(__file__)
    plt.savefig(join(path_folder, "fig/apprentissage_fcnn.pdf"))
    plt.show()

##############################################################################
############################## EXECUTION DU CODE #############################
##############################################################################

def main(n_obs, n_step, ratio):
    """Créer le jeu de donnée artificel.
    Etiquette ces données en regardant la distance moyenne d'un point à ses voisins.
    Puis entraine un classificateur knn sur le jeu de donnée d'entrainement et évlue les performances sur le jeu de données test."""
    # Création d'une base de donnée artificielle
    X, Y = generate_data(n_obs, n_step, ratio)
    
    # Entrainement d'un modèle d'intelligence artificielle
    y = to_categorical(Y) # Transforme les étiquettes en vecteur de dimension 2 (il y deux classes)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2) # Sépare en deux jeu de données entrainement et test
    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.3) # Sépare en deux jeu de données entrainement et validation
    validation_data = (X_val, y_val)
    model, norm, history = train(X_train, y_train, validation_data) # Entraine le modèle
    y_pred = test(model, norm, X_test) # Test le modèle
    
    # Plots
    plot(X, Y, history)
    plot_confusion_matrix(y_test, y_pred) # Affiche les performances du modèle

    return model, norm, X, y, history

if __name__ == "__main__":
    # Paramètre à régler
    n_obs = int(1e5) # Nombre d'observation dans la base de donnée
    n_step = int(1e3) # Nombre de pas de temps de chaque observation
    ratio = 0.3 # Proportion de série temporelle provenant de systèmes défectueux
    
    # Exécution de l'algo
    model, norm, X, y, history = main(n_obs, n_step, ratio)