# D�tection automatique de syst�me d�fectueux <img src="../../../../img/python.png" width="25"/>

**ATTENTION**
TP non termin�. Il faudrait des donn�es issues d'un syst�me r�el.

## Plateforme utilis�e
Vous pouvez utiliser :ing).
- Votre propore installation python <img src="../../../../img/python.png" height="15"/> et les fichiers de ce r�pertoire.
Voir page d'acceuil pour les conseils d'installation: [ici](https://gitlab.com/Guenole.cherot/liesse/-/tree/master/Travaux%20pratiques).

## Points du programme abord�s
- [x] R�seau de neurones
- [x] Classification
- [x] Apprentissage supervis�
- [x] Couche d'entr�e, cach�e, de sortie
- [x] Sur-apprentissage

## D�roulement de la s�ance

### Contexte
On dispose d'un syst�me constitu� d�un assemblage de pi�ces, par exemple le Control�X.
Certaines de ces pi�ces sont en mouvement ou peuvent �tre soumises � des efforts intenses.
Il est n�cessaire de les surveiller et des les r�parer si elle venait � �tre d�faillante.
Malheureusement, il n�est pas possible d�instrumenter toutes les parties d�un syst�me. Ce serait trop on�reux.
Une strat�gie viable peut consister � surveiller l��volution temporelles de certains signaux. D�s qu�une anomalie est d�tect�, un op�rateur peut arr�ter le syst�me pour chercher la source de cette anomalie.

Pour ce TP, nous consid�rerons **une vis mal serr�e (ou qui se desserre) sur une masse du pendule**.

### Cr�ation de la base de donn�es
Afin de pouvoir classifier correctement les donn�es aberrantes, l'algorithme doit disposer d'une base de donn�es d'exemple.
C'est � partir de ces exemples qu'il pourra **imiter** les d�cisions sur des donn�es jamais vues.

*Remarque :* J'ai utilis� le terme **imiter** de mani�re volontaire.
Les algorithmes d'apprentissage automatique, en particulier les algorithmes de Machine Learning, ne sont pas capables d'innovation.
Leur but est d'automatiser une t�che faisable par un humain mais fastidieuse.  
De tr�s rares exemples d'innovation existent dans une branche tr�s sp�cifique de l'IA : L'apprentissage par renforcement.
Les algorithmes et les notions n�cessaires � la compr�hension de ces algorithmes d�passent largement le cadre des activit�s propos�es sur ce git.

#### Collecte de donn�e
Il faut commencer par r�colter les donn�es sur le syst�me r�el, ou utiliser des donn�es synth�tiques g�n�r�es dans le code.

**Donn�es Synth�tiques** : 
Pour l'instant, les donn�es sont g�n�r�es synth�tiquement.
On suppose que le chariot � un mouvement al�atoire le long de son axe.
Si la vis se dessert, elle va g�n�rer des oscillations (hypoth�se) de faible amplitude.
On cherche � d�tecter ces oscillations.

**Donn�es r�elles** :
Pour collecter des donn�es r�elles, il suffit d�imposer une consigne al�atoire au moteur contr�lant et le chariot et d�observer le d�placement.
Une partie des enregistrements est effectu� avec la vis bien serr�. Une autre avec la vis desserr�e. Les r�sultats peuvent �tre tr�s diff�rent de l�application avec les donn�es synth�tiques.

#### Limite de la d�marche :
Dans cette activit�, nous avons entrain� un r�seau de neurones � d�tecter un type tr�s sp�cifique d�anomalie.
Rien ne dit qu�un autre type d�anomalie serait d�tect�, il ne le serait probablement pas d�ailleurs.
Cela souligne l�un des grands probl�mes du d�ploiement de l�intelligence artificielle dans le monde r�el : il faut une base de donn�es repr�sentative que ce que l�on souhaite automatiser.
Dans le cas de ce TP, il faudrait faire l'inventaire de toute les anomalies possibles.
Puis faire un enregistrement de ces anomalies, en esp�rant que notre mode d�enregistrement de diff�re pas trop de celui qui sera utilis� par l�utilisateur final :
-   Type de capteur,
-   Bruit de mesure
-   Effet conjoint de plusieurs d�fauts
-   �

### Interpr�tation des r�sultats
#### 1�re figure
Montre l'�volution temporelle des signaux.
Il n'est possible de voir � l'oeil nu, quelle s�rie est d�fecteuse (rouge) et laquelle est normale (vert).

#### 2�me figure
Transform�e de Fourier des s�ries temporelles.
Il n'est possible de voir � l'oeil nu, quelle s�rie est d�fecteuse (rouge) et laquelle est normale (vert).

#### Autres figures
Evolution des diff�rentes m�triques au cours de l'apprentissage.
Nous invitons le lecteur � lire l'article 3EI d'Avril 2022 sur le sujet.

Lors de la phase d�apprentissage, l�algorithme cherche � minimiser la � loss � , c�est-�-dire la valeur de la fonction objectif.
Sur la base de donn�es d�entrainement, cette valeur est strictement d�croissante : c�est normal, c�est comme cela que le r�seau de neurones s�am�liore.
En revanche, la valeur de fonction objectif passe par un minimum sur la base de donn�es de test.
C�est � ce moment que le r�seau est le meilleur. Apr�s il cherche trop � � coller � aux donn�es de la base de donn�e d�entrainement et perd en g�n�ralit�.
*Il faut donc arr�ter l�entrainement quand la fonction objectif passe par un minimum sur la base de donn�es de test.*

#### Matrice de confusion
On peut interpr�ter le r�sultat avec la matrice de confusion.
Cet outil de permet de d�terminer l� o� l'algorithme se trompe.
En particulier on regarde les quatre possibilit�es suivantes :
- Vrai positif : l'algorithme devait trouver un d�faut et l'a trouv�e
- Vrai n�gatif : l'algorithme devait trouver un fonctionnement normal et l'a trouv�e
- Faux positif : l'algorithme devait trouver un fonctionnement normal et a trouv� un d�faut
- Faux n�gatif : l'algorithme devait trouver un d�faut et a trouv� un fonctionnement normal