# Segmentation <img src="../../../../img/python.png" height="25"/> <img src="../../../../img/colab.png" height="25"/>

## Plateforme utilis�e
Vous pouvez utiliser :
- Google Colab <img src="../../../../img/colab.png" height="15"/> et ses serveurs (Aucune installation n�cessaire) : [lien](https://colab.research.google.com/drive/100AjTuoPdB81xMRVK3nWzFFUoqRdAqLC?usp=sharing).
- Votre propore installation python <img src="../../../../img/python.png" height="15"/> et les fichiers de ce r�pertoire.
Voir page d'acceuil pour les conseils d'installation: [ici](https://gitlab.com/Guenole.cherot/liesse/-/tree/master/Travaux%20pratiques).

## But du TP
Ce TP propose de visualiser le fonctionnement d'un r�seau convolutif r�alisant de la segmentation d'image.
Le r�seau est entrain� sur des images de circulation et doit reconnaitre les parties de l'image correspondant � une route, un trottoire, un pi�ton, un cycliste ...

## Points du programme abord�s
- [x] Classification
- [x] R�seau de neurones