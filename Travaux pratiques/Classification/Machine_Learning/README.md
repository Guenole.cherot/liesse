# Détection automatique de valeurs aberrantes  <img src="../../../img/python.png" width="25"/> <img src="../../../img/colab.png" width="25"/>

## Plateforme utilisée
Vous pouvez utiliser :
- Google Colab <img src="../../../../img/colab.png" height="15"/> et ses serveurs (Aucune installation nécessaire) : [lien](https://colab.research.google.com/drive/1CJzIdruALo9dWZBt-om3SU-o2FMwTgQV?usp=sharing).
- Votre propore installation python <img src="../../../../img/python.png" height="15"/> et les fichiers de ce répertoire.
Voir page d'acceuil pour les conseils d'installation: [ici](https://gitlab.com/Guenole.cherot/liesse/-/tree/master/Travaux%20pratiques).

## Points du programme abordés
- [x] KNN
- [x] Classification
- [x] Apprentissage non-supervisé
- [x] Apprentissage supervisé
- [x] Matrice de confusion

## Déroulement de la séance

### Contexte
Supposons disposer d'un capteur dont les valeurs sont parfois aberrantes.
Nous aimerions filtrer de manière automatiques ces valeurs.
De nombreuses solutions utilisant du traitement de signal existent mais le but de ce répertoire est de mettre en application des concepts simples d'apprentissage données (ou Intelligence Artificielle).

## Création de la base de données
Afin de pouvoir classifier correctement les données aberrantes, l'algorithme doit disposer d'une base de données d'exemple.
C'est à partir de ces exemples qu'il pourra **imiter** les décisions sur des données jamais vues.

*Remarque :* J'ai utilisé le terme **imiter** de manière volontaire.
Les algorithmes d'apprentissage automatique, en particulier les algorithmes de Machine Learning, ne sont pas capables d'innovation.
Leur but est d'automatiser une tâche faisable par un humain mais fastidieuse.  
De très rares exemples d'innovation existent dans une branche très spécifique de l'IA : L'apprentissage par renforcement.
Les algorithmes et les notions nécessaires à la compréhension de ces algorithmes dépassent largement le cadre des activités proposées sur ce git.

### Collecte de donnée
Il faut commencer par récolter les données sur le système réel, ou utiliser des données synthétiques générées dans le code.
On dispose ainsi d'une série temporelle d'enregistrements successifs des données issue d'un capteur.

### Principe de la détection de valeur aberrante
Les données récoltées ont une cohérence temporelle :
la mesure au pas de temps t est relativement proche de la mesure au pas de temps t+1.
Mais que signifie "relativement proche" ?
Pour le comprendre nous allons devoir transformer les données en notre possession.
Cette phase, appelée "pré-traitement" ou "extraction de caractéristique" est souvent nécessaire pour obtenir de bons résultats en Machine Learning.

*Remarque* :
La phase de pré-traitement peut prendre de nombreuses formes.
Les manipulations effectuées ici sont spécifiques à l'application.

A partir de la série temporelle possédant *n* valeurs, nous allons créer *n-1* observations.
Chaque observation, est la concaténation de valeurs consécutives de la base de données.
Par exemple, si la série temporelle est *[0, 1, 2, 4 ,4]*, alors la base de données sera composée de 4 observations :
- *[0, 1]*
- *[1, 2]*
- *[2, 4]*
- *[4, 4]*

Il est alors possible de placer chacun de ces points sur un plan (en 2 dimensions).
Ensuite, on regarde la distance moyenne d'un point à ses *x* voisins.
De cette façon, on sait si un point est proche des voisins relativement aux autres.
Les points qui sont manifestement plus éloignés sont probablement des valeurs aberrantes.
Il suffit alors de les **labéliser** comme des points aberrants : notre base de données est créée.

Ce type de labélisation est dit non supervisé car on ne labélise pas explicitement les observations de la base de données.
On a créé un algorithme  qui sait simplement qu'il faut diviser la base de données en deux groupes et qui affecte des étiquette sur la base de critère objectif (comme la distance entre les points).

Le code "Labelisation_non_supervise.py" génère l'image "fig/non-supervise.pdf".

## Entrainement de l'algorithme de détection
On possède désormais un base de données contenant :
- Des observations de dimension 2 contenants deux mesures successives
- Des étiquettes (ou label) précisant si une observation est aberrante ou non

Il reste à entrainer l'algorithme *K plus proche voisin* dans le but de tracer la frontière entre les deux classes.
Pour cela, pour chaque point de l'espace, on regarde les K plus proche voisins et on compte le nombre de chaque classe.
Si il y a plus de points normaux que d'aberrants alors le point doit être normal.
Si c'est l'inverse, alors il est probablement aberrant.
C'est tout !

On peut interpréter le résultat avec la matrice de confusion.
Cet outil de permet de déterminer où l'algorithme se trompe.
En particulier on regarde les quatre possibilités suivantes :
- Vrai positif : l'algorithme devait trouver une valeur aberrante et l'a trouvée
- Vrai négatif : l'algorithme devait trouver une valeur normale et l'a trouvée
- Faux positif : l'algorithme devait trouver une valeur normale et a trouvé une valeur aberrante
- Faux négatif : l'algorithme devait trouver une valeur aberrante et a trouvé une valeur normale

Le code "Apprentissage.py" génère l'image "fig/apprentissage_knn.pdf".