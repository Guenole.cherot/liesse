# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 17:02:32 2022

@author: Guenole
"""
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from sklearn.neighbors import NearestNeighbors

from os.path import join, dirname
import os

# Reproducibility
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
rs = RandomState(MT19937(SeedSequence(0))) # Fixe l'aléatoire (Toujours le même réultat aléatoire)

##############################################################################
################################ COEUR DU CODE ###############################
##############################################################################

def generate_data(n_sample, ratio):
    """Création de la série temporelle issue d'un capteur
    et ajout de valeur abérante de manière aléatoire."""
    n_outliar = int(n_sample*ratio) # Nombre de donnée corompu
    x = 0
    X = np.zeros(n_sample) # Vecteur qui représente l'échantillonnage du capteur
    rand = rs.normal(size=n_sample) # Donnée qui vont être ajouté à chaque pas temps pour que X soit un mouvement brownien
    
    for k in range(n_sample): # Pour chaque pas de temps
        x += rand[k] # La mesure au pas de temps t vaut celle au pas de temps t-1 plus un incrément aléatoire
        X[k] = x 
        
    # Ajout de valeurs abérantes
    outliar_label = rs.choice(range(n_sample-1), size = n_outliar, replace=False) # Création de l'emplacement des valeurs abérantes
    liar = rs.normal(loc = 1, scale = 5, size=n_outliar) # Amplitude des valeurs abérantes
    X[outliar_label] = liar # Ajout des valeurs abérante à la série temporelle
    
    # Création du vecteur d'état (capteur au pas de temps t et t-1)
    Xt = X[:-1]
    Xt_1 = X[1:]
    X = np.stack((Xt, Xt_1), axis=1)
    outliar_label = np.unique(np.concatenate((outliar_label-1,outliar_label), axis=0)) # Comme une observation est de dimention 2 (capteur à t et t-1) il faut que les valeurs abérantes comptent sur plusieurs pas de temps.
    label = np.zeros(len(Xt))
    label[outliar_label] = 1
    
    return X, label

def unsupervised_labeling(X, n_neighbors, threshold):
    """Labélisation non supervisé des données issue du capteur.
    Cette étape peut mal labéliser."""
    # Entrainement du knn
    nbrs = NearestNeighbors(n_neighbors=n_neighbors, algorithm='ball_tree').fit(X) # Cherche les n plus proches voisins
    distances, indices = nbrs.kneighbors(X) # Distance de chaque point aux n plus proche voisins
    distances = distances.mean(axis=1) # Distance moyenne au n voisins
    
    # labellisation 
    outliar = np.where(distances >= threshold)[0] # Points considérés comme abérant car trop loin des autres
    unsupervised_label = np.zeros(len(distances)) # Création d'un vecteur label
    unsupervised_label[outliar] = 1 # 0 pour les données valide, 1 pour les données abérrantes

    return distances, unsupervised_label

def save_data(X, Y):
    """Sauvegarde les données sous la forme d'une base de données classique"""
    path_folder = dirname(__file__)
    os.makedirs(join(path_folder, "data"), exist_ok=True) # Créer le répertoire /data s'il n'existe pas
    np.save(join(path_folder, "data/X.npy"), X) # Sauvegarde les données
    np.save(join(path_folder, "data/Y.npy"), Y) # Sauvegarde les étiquettes
    

##############################################################################
################################## FIGURES ###################################
##############################################################################

def plot_time_serie(ax, X, t_max=500):
    """Affiche la série temporelle généré artificiellement."""
    ax.plot(X[0:t_max]) # Série temporelle entre 0 et pas de temps t_max
    ax.set_ylabel("Mesure issue du capteur")
    ax.set_xlabel("Temps (it)")
    ax.set_xlim(xmin=0, xmax=t_max)
    ax.grid(True)
    
def plot_time_serie_labeled(ax, X, unsupervised_label, t_max=500):
    """Affiche la série temporelle généré artificiellement."""
    normal = np.where(unsupervised_label == 0) # Indices où les données sont supposées normal
    aberrant = np.where(unsupervised_label == 1) # Indice où les données sont supposées aberrantes
    cmap =  matplotlib.colors.ListedColormap(["darkgreen", "darkred"]) # Couleurs utilisées
    X1 = np.zeros(X.shape)
    X1[:] = np.nan
    X2 = np.zeros(X.shape)
    X2[:] = np.nan
    
    X1[normal] = X[normal]
    X2[aberrant] = X[aberrant]
    
    ax.plot(X1[0:t_max], color=cmap(0)) # Série temporelle entre 0 et pas de temps t_max
    ax.plot(X2[0:t_max], color=cmap(1))
    ax.set_ylabel("Mesure issue du capteur")
    ax.set_xlabel("Temps (it)")
    ax.set_xlim(xmin=0, xmax=t_max)
    ax.grid(True)

def plot_distances_2D(ax, X):
    ax.scatter(X[:,0], X[:,1])
    ax.set_xlabel("Valeur mesuré par le capteur à t - dt")
    ax.set_ylabel("Valeur mesuré par le capteur à t")

def plot_distances(ax, distances):
    """Figure illustrant la distance des points les uns aux autres"""
    ax.bar(np.arange(0, len(distances), 1), distances, width=len(distances)/500)
    ax.axhspan(threshold, max(distances), alpha=0.2, color="red", label="Seuil détection données abérantes") # Bandeau contenant les points abérants (trop éloignés des autres)
    ax.set_ylabel("Distance moyenne aux {0} voisins".format(n_neighbors))
    ax.set_xlabel("Identifiant de l'échantillon")
    ax.legend()
    ax.set_xlim(xmin=0, xmax=len(distances))
    ax.grid(True)
    
def plot_data(ax, X, y, xlabel="Axe 1", ylabel="Axe 2", title="Visualisation base de donnée", label0="Label : 0", label1="Label : 1", colorpalet=1):
    """Affiche les erreurs commise lors de la labélisation non supervisé."""
    if colorpalet :
        cmap =  matplotlib.colors.ListedColormap(["C0", "gold"]) # Couleurs utilisées pour l'apprentissage
    else :
        cmap =  matplotlib.colors.ListedColormap(["darkgreen", "darkred"]) # Couleurs utilisées pour la labélisation non supervisée
    class1 = np.where(y==1) # Position des observations de classe 1
    class0 = np.where(y==0) # Position des observation de classe 0
    
    ax.scatter(X[class0,0], X[class0,1], color=cmap(0), label=label0) # Affichage des observations de la classe 0 en 2D
    ax.scatter(X[class1,0], X[class1,1], color=cmap(1), label=label1) # Affichage des observations de la classe 1 en 2D
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.legend()
    ax.set_title(title)
    
def plot_unsupervised_labeling(distances, X, unsupervised_label, label):
    """Affiche les résultats de la labellisation non supervisé."""
    path_folder = dirname(__file__)
    
    gs = gridspec.GridSpec(4, 4)
    fig = plt.figure(figsize=(8,13), dpi=300)
    
    ax0 = fig.add_subplot(gs[0, :]) # ligne 0, toute les colonnes
    ax1 = fig.add_subplot(gs[1, 0:2]) # ligne 1, colonne 0
    ax2 = fig.add_subplot(gs[1, 2:])
    ax3 = fig.add_subplot(gs[2, 1:3])
    ax4 = fig.add_subplot(gs[3, :])
    
    plot_time_serie(ax0, X[:,0])
    plot_distances_2D(ax1, X)
    plot_distances(ax2, distances)
    plot_data(ax3, X, unsupervised_label,
              xlabel="Valeur mesuré par le capteur à t - dt",
              ylabel="Valeur mesuré par le capteur à t",
              label0="Normal",
              label1="Aberrant",
              title="Visualisation de la détection non supervisée", colorpalet=0)
    plot_time_serie_labeled(ax4, X[:,0], unsupervised_label)
    fig.suptitle("Donnée générées aléatoirements et étiquetage des données de manière non supervisée",
                 fontsize = 13)
    plt.tight_layout()
    plt.savefig(join(path_folder, "fig/non-supervise.pdf"))
    plt.show()
    
##############################################################################
############################## EXECUTION DU CODE #############################
##############################################################################

def main(n_sample, ratio, n_neighbors, threshold):
    """Créer le jeu de donnée artificel.
    Etiquette ces données en regardant la distance moyenne d'un point à ses voisins."""
    # Création d'une base de donnée artificielle
    X, label = generate_data(n_sample, ratio) # Génération de donnée
    distances, unsupervised_label = unsupervised_labeling(X, n_neighbors, threshold) # Créer des défauts de manière artificielle
    plot_unsupervised_labeling(distances, X, unsupervised_label, label) # Affiche les résultats de la labélisation
    save_data(X, unsupervised_label) # Sauvegarde la base de données
    

if __name__ == "__main__":
    # Paramètre à régler
    n_sample = int(5e3) # Nombre de pas de temps
    ratio = 0.02 # Proportion de valeur abérante
    n_neighbors = 20 # Nombre de voisin à considérer pour calculer la distance
    threshold = 2 # Seuil à partir duquel un point est considéré trop éloigné de ses voisins
    
    # Exécution de l'algo
    main(n_sample, ratio, n_neighbors, threshold)
    
    