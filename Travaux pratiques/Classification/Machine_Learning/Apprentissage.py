# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 17:02:32 2022

@author: Guenole
"""
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from os.path import join, dirname

from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics._classification import confusion_matrix

from Labelisation_non_supervise import plot_data

# Reproducibility
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
rs = RandomState(MT19937(SeedSequence(0))) # Fixe l'aléatoire (Toujours le même réultat aléatoire)

##############################################################################
################################ COEUR DU CODE ###############################
##############################################################################

def load_data():
    """Charge la base de donnée en mémoire."""
    path_folder = dirname(__file__)
    X = np.load(join(path_folder, "data/X.npy")) # Charge les données
    y = np.load(join(path_folder, "data/Y.npy")) # Charge les étiquettes (labels)
    return X, y

def fit(X, y, n_neighbors=3):
    """Créer un classificateur knn grace à la base de données synthétique."""
    clf = KNeighborsClassifier(n_neighbors=n_neighbors) # Considère les n_neighbors plus proche voisins
    clf.fit(X, y) # Entraine le modèle (très rapide car KNN est un algorithme simple)
    return clf

    
##############################################################################
################################## FIGURES ###################################
##############################################################################
    
def plot_unsupervised_error(ax, X, false_positive, false_negative, true_positive, true_negative):
    """Affiche les erreurs commise lors de la labélisation non supervisé."""
    cmap_bold =  matplotlib.colors.ListedColormap(["salmon", "darkred", "lightgreen", "darkgreen"]) # Couleurs utilisées
    
    ax.scatter(X[true_negative,0], X[true_negative,1], color=cmap_bold(3), label="Vrais négatifs", linewidths=0.5, edgecolor="black")
    ax.scatter(X[true_positive,0], X[true_positive,1], color=cmap_bold(2), label="Vrais positifs", linewidths=0.5, edgecolor="black")
    ax.scatter(X[false_negative,0], X[false_negative,1], color=cmap_bold(1), label="Faux négatifs", linewidths=0.5, edgecolor="black")
    ax.scatter(X[false_positive,0], X[false_positive,1], color=cmap_bold(0), label="Faux positifs", linewidths=0.5, edgecolor="black")
    
    ax.set_xlabel("Valeur mesuré par le capteur à t - dt")
    ax.set_ylabel("Valeur mesuré par le capteur à t")
    ax.legend()
    
def plot_confusion_matrix(ax, y_true, y_pred):
    """Affiche la matrice de confusion pour l'apprentissage non superviser.
    Il faut noter qu'on ne peut la tracer que parqu'on utilise des données synthétiques."""
    cm = confusion_matrix(y_true, y_pred,
                          sample_weight=None, labels=None,
                          normalize='true')
    display_labels = unique_labels(y_true, y_pred)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=display_labels)
   
    return disp.plot(ax=ax, cmap="RdYlGn")

def plot_decision(ax, X, y, clf, train=False):
    """Affiche la zone de décision du knn.
    Les points verts sont normaux et bien classifiés,
    Les points rouges sont abérants et bien classifiés,
    Les points jaunes sont normaux ou abérant et mal classifiés."""
    cmap_light = matplotlib.colors.ListedColormap(["lightblue", "antiquewhite"]) # Couleurs utilisées pour le fond
    cmap =  matplotlib.colors.ListedColormap(["gold", "C0"]) # Couleurs utilisées pour les points
    
    h = 0.2
    # Affiche la frontière de décision. Pour cela, on assigne à chaque point du cadrillage une couleur
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1 # Bornes x du quadrillage
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1 # Bornes y du quadrillage
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h)) # Créer la grille
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()]) # Assigne une couleur à chaque point en se servant du modèle ayant appris
    Z = Z.reshape(xx.shape) # Change la dimension
    ax.contourf(xx, yy, Z, cmap=cmap_light) # Affiche le résultat sur l'ax
    
    class0 = np.where(y==0)
    class1 = np.where(y==1)
    
    if train :
        ax.scatter(X[class0,0], X[class0,1], color=cmap(1), label="label : 0")
        ax.scatter(X[class1,0], X[class1,1], color=cmap(0), label="label : 1")
        ax.set_title("Entrainement de l'algorithme KNN")
    else :
        y_pred = clf.predict(X)
        false_positive, false_negative, true_positive, true_negative = compute_label_error(y_pred, y)
        plot_unsupervised_error(ax, X, false_positive, false_negative, true_positive, true_negative)
        ax.set_title("Test l'algorithme KNN")
    
    ax.set_xlabel("Axe 1")
    ax.set_ylabel("Axe 2")
    
    ax.legend()
    
def compute_label_error(unsupervised_label, label, for_time_serie=False):
    """Calcul les errers commises par la labélisation non supervisé.
    Il n'est pas possible de la faire dans le cas ou les données ne sont pas synthétiques."""
    unsupervised_true_label = np.where(unsupervised_label==1)[0]
    unsupervised_false_label = np.where(unsupervised_label==0)[0]
    true_label = np.where(label==1)[0]
    false_label = np.where(label==0)[0]
    
    false_positive = unsupervised_true_label[np.where(np.isin(unsupervised_true_label, false_label))] # Faux négatif (on pense que les données sont bonne mais elles ne le sont pas)
    false_negative = unsupervised_false_label[np.where(np.isin(unsupervised_false_label, true_label))] # Faux positif (On pense que les données sont corrompu mais elle ne le sont pas)
    true_positive = unsupervised_true_label[np.where(np.isin(unsupervised_true_label, true_label))] # Bien labélisé "normale"
    true_negative = unsupervised_false_label[np.where(np.isin(unsupervised_false_label, false_label))] # Bien labélisé "abérant"
    
    if for_time_serie: # Add the next point to the index if we consider time serie data
        false_positive = np.unique(np.concatenate((false_positive+1,false_positive), axis=0))
        false_negative = np.unique(np.concatenate((false_negative+1,false_negative), axis=0))
        true_positive = np.unique(np.concatenate((true_positive+1,true_positive), axis=0))
        true_negative = np.unique(np.concatenate((false_positive+1,true_negative), axis=0))
    
    return false_positive, false_negative, true_positive, true_negative


    
def plot(X, y, X_train, X_test, y_train, y_test, clf):
    """Plot la figure montrant le processus d'entrainement."""
    gs = gridspec.GridSpec(2, 2)
    fig = plt.figure(figsize=(9,7), dpi=300)
    
    ax0 = fig.add_subplot(gs[0, 0]) # ligne 0, colonne 0
    ax1 = fig.add_subplot(gs[0, 1]) # ligne 0, colonne 1
    ax2 = fig.add_subplot(gs[1, 0])
    ax3 = fig.add_subplot(gs[1, 1])
    
    plot_data(ax0, X, y)
    plot_decision(ax1, X_train, y_train, clf, train=True)
    plot_decision(ax2, X_test, y_test, clf)
    y_pred = clf.predict(X_test)
    plot_confusion_matrix(ax3, y_test, y_pred)
    
    plt.tight_layout()
    # fig.suptitle("Processus d'entrainement")
    path_folder = dirname(__file__)
    plt.savefig(join(path_folder, "fig/apprentissage_knn.pdf"))
    plt.plot()
    
##############################################################################
############################## EXECUTION DU CODE #############################
##############################################################################

def main(n_neighbors):
    """Entraine un classificateur knn sur le jeu de donnée d'entrainement et 
    évalue les performances sur le jeu de données test."""

    # Entrainement de l'algorithme knn pour la classification de données abérantes
    X, y = load_data() # Charge les données issue de la labelisation non supervisée
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42) # Séparation entrainement validation
    clf = fit(X_train, y_train, n_neighbors) # Entrainement du classificateur sur la base de donnée d'entrainement
    plot(X, y, X_train, X_test, y_train, y_test, clf)

if __name__ == "__main__":
    # Paramètre à régler
    n_neighbors = 3 # Nombre de voisin à considérer pour calculer la frontière de décision de l'algorithme KNN
    
    # Exécution de l'algo
    main(n_neighbors)
    
    