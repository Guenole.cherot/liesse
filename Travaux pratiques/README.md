# Travaux pratiques
Codes permettants de répliquer les exemples utilisé lors de la formation.

Les codes sont disponibles en trois versions, indiqué par les vignettes suivantes :
- <img src="../img/python.png" width="25"/> code disponible sur Python
- <img src="../img/matlab.png" width="25"/> code disponible sur Matlab
- <img src="../img/colab.png" width="25"/> code disponible sur Google Colab (notebook python sans installation)

# Installation des logicielles
## Python <img src="../img/python.png" width="25"/>

1. Télécharger le dossier GitLab (via Git clone ou en .zip)
2. Installer **python** et un gestionnaire de paquet. Je vous conseille **Anaconda** : [lien](https://www.anaconda.com/)
3. Une fois anaconda installé, ouvrir *Anaconda prompt* (Windows) ou *command prompt* (Linux, Mac)
4. Se placer dans le dossier *Liesse* que vous venez de télécharger sur git (`cd "chemin_vers_Liesse"`)
5. Créer un nouvel environnement dans lequel seront installées toutes les librairies : `conda env create -f environment.yml`
6. Activer cet environnement : `conda activate Liesse`
7. Installer un éditeur de code : `conda install spyder`
8. Lancer l'éditeur : `spyder`

Les prochaines fois, l'installation sera déjà faite.
Vous n'aurez qu'à refaire les étape 3, 6 et 8 pour commencer le TP.

## Matlab <img src="../img/matlab.png" width="25"/>
- Version 2022a ou plus
- Librairies :
	- Simulink
	- Statistics and machine learning Toolbox
	- Reinforcement Learning Toolbox

## Google Colab <img src="../img/colab.png" width="25"/>
Pas besoin d'une installation python.
Tout est en ligne sur les serveurs Google.
Nécessité d'un compte Google.

# Liste des TP proposés
1. Classification
	1. Machine Learning : K plus proche voisin <img src="../img/python.png" height="18"/>
	2. Réseau de neurones :
	    1. Détection de défaut <img src="../img/python.png" height="18"/>
	    2. Classification de chiffres en écriture manuscrite <img src="../img/python.png" height="18"/> <img src="../img/colab.png" height="18"/>
	    3. Segmentation d'image routière pour la conduite autonome <img src="../../../img/python.png" height="18"/> <img src="../img/colab.png" height="18"/>
2. Régression
	1. Machine Learning : Régression linéaire simple et multiple <img src="../img/matlab.png" height="18"/>
	2. Réseau de neurones <img src="../img/matlab.png" height="18"/> <img src="../img/python.png" height="18"/> <img src="../img/colab.png" height="18"/>
3. Introduction à l'éthique des données <img src="../img/python.png" height="18"/>