# Pr�diction de la force de frottement sur un chariot � partir de la vitesse impos�e <img src="../../../../img/python.png" height="25"/> <img src="../../../../img/colab.png" height="25"/> <img src="../../../../img/matlab.png" height="25"/>

## Plateforme utilis�e
Vous pouvez utiliser :
- Google Colab <img src="../../../../img/colab.png" height="15"/> et ses serveurs (Aucune installation n�cessaire) : [lien](https://colab.research.google.com/drive/1wNnpg8Vtp6hzD5dekccd7rYvmtfF4jNH?usp=sharing).
- Votre propore installation python <img src="../../../../img/python.png" height="15"/> et les fichiers de ce r�pertoire.
Voir page d'acceuil pour les conseils d'installation: [ici](https://gitlab.com/Guenole.cherot/liesse/-/tree/master/Travaux%20pratiques).
- Matlab <img src="../../../../img/matlab.png" height="15"/> via le TP [partie A](../Machine Learning) (Une partie traite des r�seaux de neurones).

## But du TP
Ce TP est la suite de la [partie A](../Machine Learning) impl�ment�e sur Matlab par Gu�nol� Cherot.
Dans cette partie, nous allons effectuer la regression avec un r�seau de neurones en python avec la librairie Keras.

Un syst�me en mouvement est soumis � des frottements.
Ces frottements sont rarement fournis par les constructeurs.
Il faut donc les mod�liser et effectuer des exp�riences sur le syst�me pour faire correspondre le mod�le aux observations. 
Pour cette activit� pratique, nous d�cidons de nous affranchir des mod�les th�oriques et de les remplacer par mod�le apris via un apprentissage automatique.

Le but est donc d'apprendre un mod�le tel que : f(vitesse) = frottement

### Critiques de l'activit� de TP
Les mod�les � partir de r�seau de neurones ne sont pas r�ellement adapt� pour un probl�me simple comme celui trait� ici.
Le but de cette activit� est simplement de montrer qu'il est possible d'utiliser un r�seau comme une bo�te noire prenant **Nx** variables d'entr�es et donnant **Ny** variables de sortie.
Une am�lioration de ce TP sera de chercher � lier plusieurs grandeurs d'entr� � plusieurs grandeurs de sortie.

En pratique, les r�seaux de neurones profonds sont utilis�s pour des t�ches bien plus complexe.
Mais ils sont aussi plus difficiles � entrainer.


###  Points du programme abord�s
- [x] R�gression
- [x] Apprentissage supervis�
- [x] R�seau de neurones