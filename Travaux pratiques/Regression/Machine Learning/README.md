# Pr�diction de la force de frottement sur un chariot � partir de la vitesse impos�e <img src="../../../img/matlab.png" width="25"/>

## Plateforme utilis�e
Matlab <img src="../../../img/matlab.png" height="18"/> voir versions et paquets [ici](https://gitlab.com/Guenole.cherot/liesse/-/tree/master/Travaux%20pratiques).

## But du TP
Un syst�me en mouvement est soumis � des frottements.
Ces frottements sont rarement fournis par les constructeurs.
Il faut donc les mod�liser et effectuer des exp�riences sur le syst�me pour faire correspondre le mod�le aux observations. 
Pour cette activit� pratique, nous d�cidons de nous affranchir des mod�les th�oriques et de les remplacer par mod�le apris via un apprentissage automatique.

Le but est donc d'apprendre un mod�le tel que : f(vitesse) = frottement

## D�roulement du TP
Se r�f�rer � la pr�sentation [Demarche AI](Demarche AI.pdf).
Ouvir le fichier "Regression_v_to_frott.mlx" et suivre les instructions.

###  Points du programme abord�s
- [x] R�gression lin�aire
- [x] R�gression lin�aire multiple
- [x] Apprentissage supervis�