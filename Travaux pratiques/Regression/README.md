# Régression
La régression recouvre plusieurs méthodes d’analyse statistique permettant d’approcher une variable à partir d’autres qui lui sont corrélées.
En apprentissage automatique, on distingue les problèmes de régression des problèmes de classification.
Ainsi, on considère que les problèmes de prédiction d'une variable quantitative sont des problèmes de régression tandis que les problèmes de prédiction d'une variable qualitative sont des problèmes de classification.

## Liste de TP proposés
Nous verrons deux types de régression :
1. Machine Learning : Régression linéaire simple et multiple <img src="../../img/matlab.png" height="18"/>
2. Réseau de neurones <img src="../../img/matlab.png" height="18"/> <img src="../../img/python.png" height="18"/> <img src="../../img/colab.png" height="18"/>