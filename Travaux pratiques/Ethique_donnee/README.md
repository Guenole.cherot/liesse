# Introduction à l'éthique des données <img src="../../../../img/python.png" height="25"/>
Les données personnelles, c'est important. Nous le savons.
Mais pourquoi est-ce si important ?
Je vous propose une petite activité autour de votre vie privé.

## Plateforme utilisée
Vous pouvez utiliser :
- Votre propore installation python <img src="../../../../img/python.png" height="18"/> et les fichiers de ce répertoire.
Voir page d'acceuil pour les conseils d'installation: [ici](https://gitlab.com/Guenole.cherot/liesse/-/tree/master/Travaux%20pratiques).

## Derroulement
1. Rendez vous sur la page "donnée" de google : [takeout.google](https://takeout.google.com/).
2. Téléchargez l'historique des positions.
3. Placer de dossier Takeout dans le même dossier que Google.py
4. Lancer le script Google.py

## Questions
1. Saviez-vous que Google possédait ces données ?
2. Pouvez-vous montrez aux autres participant les deux cartes ? Sinon, pourquoi ?
3. Qu'est-il possible de savoir sur vous à partir de ces données ? Est-ce un danger ?
4. Pensez-vous que Google (ou une autre entreprise privé ou public) possède des données à votre égard ? Est-ce un problème ? Si oui, pourquoi ?
5. Connaissez-vous les restrictions d'utilisation de ces données ? Est-il possible de vérifier facilement que ces restrictions sont bien appliquées ?