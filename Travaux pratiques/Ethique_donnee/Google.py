# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 15:03:32 2022

@author: Guenole
"""

import json
from os.path import join
import os 
import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

def import_data():
    """Importe les données"""
    dir_path = os.path.dirname(os.path.realpath(__file__)) # Dossier emplacement de ce code
    path = "Takeout\Historique des positions" # Position des donneés
    f = open(join(dir_path, path,'Records.json')) # Ouvre le fichier .json
    locations = json.load(f)["locations"]
    return locations

def pre_process(locations):
    """Traîte les données pour récuper les coordonnés"""
    n_lines = len(locations) # Nombre de ligne de la base de données
    geo = np.zeros((n_lines,2)) # Tableau qui contient toute les données
    for k in range(n_lines): # Lie les données avec une boucle for
        geo[k,0] = locations[k]["latitudeE7"] # Donnée de latitude
        geo[k,1] = locations[k]["longitudeE7"] # Donnée de longitude
    geo /= 1e7 # Lat et long en degré
    return geo
    
def plot_monde(ax):
    m = Basemap(llcrnrlon=-170.,llcrnrlat=-58.,urcrnrlon=190.,urcrnrlat=84.,\
                rsphere=(6378137.00,6356752.3142),\
                resolution='l',projection='merc',\
                lat_0=40.,lon_0=-20.,lat_ts=20.)
    ax.set_title('Dit Google : Où ai-je voyagé dans le monde ?')
    return m

def plot_france(ax):
    m = Basemap(llcrnrlon=-6., urcrnrlon=9., 
                llcrnrlat=42., urcrnrlat=53.,\
                rsphere=(6378137.00,6356752.3142),\
                resolution='l',projection='merc',\
                lat_0=40.,lon_0=-20.,lat_ts=20.)
    ax.set_title('Dit Google : Où ai-je voyagé en France ?')
    return m

def plot(geo, where="france"):
    # Affiche les points sur la carte
    fig=plt.figure(dpi=300)
    ax=fig.add_axes([0.1,0.1,0.8,0.8])
    if where == "france":
        m = plot_france(ax)
    else :
        m = plot_monde(ax)
    m.drawcoastlines(linewidth=0.25)
    m.drawcountries(linewidth=0.25)
    m.fillcontinents(color='antiquewhite',lake_color='lightsteelblue')
    m.drawmapboundary(fill_color='lightsteelblue')
    m.scatter(geo[:,1], geo[:,0], latlon=True, color="red", s=2, zorder=10)
    
    plt.show()

def main():
    locations = import_data()
    geo = pre_process(locations)
    plot(geo, "monde")
    plot(geo, "france")
    return locations

if __name__ == "__main__":
    locations = main()