Cours d'Andrew NG sur Coursera (progression lente et claire donc long mais très approfondi):
https://www.coursera.org/learn/neural-networks-deep-learning

Livre de Aurélien Géron. Un indispensable. Il couvre tous les aspects du machine learning et tous les exemples sont illustrés avec des codes pythons. Source d'inspiration idéale pour coder.
Hands-On Machine Learning with Scikit-Learn and TensorFlow: Concepts, Tools, and Techniques to Build Intelligent Systems

Chaine Youtube 3blue1brown (très pédagogique).
https://www.youtube.com/c/3blue1brown

Site towardsdatascience.com
Plein de petits articles pour répondre à des questions sur un sujet spécifique.
Exemple : la convolution
https://towardsdatascience.com/a-comprehensive-guide-to-convolutional-neural-networks-the-eli5-way-3bd2b1164a53

Apprentissage par renforcement :
L'apprentissage par renforcement de A à Z. Pas de pré-requis. Long et très appronfondi.
Reinforcement Learning : an introduction.
http://incompleteideas.net/book/the-book.html

Contenu condensé du livre précédent. 10 vidéos d'1h30.
https://youtu.be/2pWv7GOvuf0