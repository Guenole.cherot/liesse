# Cours 1�re journn�e

## Introduction partie 1
- Attendus du programme
- Choisir sa m�thode de r�solution
    - Supervis� (R�gression / Classification)
    - Non supervis� (Groupement / R�duction de dimension)
    - Apprentissage par renforcement

## Introduction partie 2
- M�trique
- Matrice de confusion
- S�paration Entrainnement / Validation / Test
- Sur-apprentissage

## R�seau de neurones Partie I
- Quand utiliser les r�seaux de neuronnes
    - Type de donn�e (structur� / non structur�)
    - Taille du jeu de donn�es
- Neurone seul (perceptron)
    - Poids
    - Fonctions d'activation
- R�seau de neuronnes
    - Propagation (inf�rence)
    - R�tropropagation (Apprentissage)
        - Fonction "Loss"
        - Influence des poids sur la Loss
        - Calcul du gradient
- Hyperparam�tres
    - Learning rate
    - Architecture du r�seau

# Cours 2�me journn�e

## R�capitulatif jour 1

## R�seau de neurones Partie II
- Pourquoi d'autres structures ?
- Structures possibles (non exhaustif)
    - R�seau convolutif
    - R�seau r�curant

## Auto-encoder
- R�duction de dimension des donn�es (Compression image non supervis�)
- Illustration de la phase d'apprentissage

## Apprentissage par Renfocement
- Utilit� (Prise de d�cision s�quentielle : Controle-Commande)
- Formulation du probl�me
    - Action
    - Observation
    - R�compense
- Composant d'un agent par renforcement
    - Politique
    - Fonction Valeur
    - Mod�le de l'environnement

# Liste r�f�rences bibliographiques