# Cours 1�re journn�e

## Introduction partie 1
- Attendus du programme
- Choisir sa m�thode de r�solution
    - Supervis� (R�gression / Classification)
    - Non supervis� (Groupement / R�duction de dimension)
    - Apprentissage par renforcement

## Introduction partie 2
- M�trique
- Matrice de confusion
- S�paration Entrainnement / Validation / Test
- Sur-apprentissage

## R�seau de neurones Partie I
- Quand utiliser les r�seaux de neuronnes
    - Type de donn�e (structur� / non structur�)
    - Taille du jeu de donn�es
- Neurone seul (perceptron)
    - Poids
    - Fonctions d'activation
- R�seau de neuronnes
    - Propagation (inf�rence)
    - R�tropropagation (Apprentissage)
        - Fonction "Loss"
        - Influence des poids sur la Loss
        - Calcul du gradient
- Hyperparam�tres
    - Learning rate
    - Architecture du r�seau